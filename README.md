# trellis-boilerplate

Trellis-Bedrock boilerplate

# Features
- Trellis + Bedrock included
- Encrypted backup (example s3) for production with https://github.com/Xilonz/trellis-backup-role
- Trellis auto deploy https://gitlab.com/alex-galey/trellis-auto-deploy/

## Pulling from upstream
```git subtree pull --prefix bedrock https://github.com/roots/bedrock master --squash```
```git subtree pull --prefix trellis https://github.com/roots/trellis master --squash```
