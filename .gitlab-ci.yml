# A pipeline is composed of independent jobs that run scripts, grouped into stages.
# Stages run in sequential order, but jobs within stages run in parallel.
#
# For more information, see: https://docs.gitlab.com/ee/ci/yaml/index.html#stages

image: willhallonline/ansible:2.12-alpine-3.14

before_script:
  - whoami
  - git submodule update --init
  - ansible --version
  - ansible-lint --version
  - export ANSIBLE_HOST_KEY_CHECKING=False # disable host key checking for ansible
  - chmod -R 755 trellis
  - cd trellis
  - mkdir ~/.ssh
  - eval `ssh-agent -s`
  - echo "${ANSIBLE_SSHKEY}" |  tr -d ' ' | base64 -d | ssh-add -
  - echo "$VAULT_PASS" > .vault_pass

stages:
  - prestaging
  - preprod
  - provision
  - deploystaging
  - deployprod

prestaging:
  stage: prestaging
  script:
    - ansible --inventory hosts/staging all -m ping
  rules:
    - if: '$CI_PIPELINE_SOURCE == "schedule"'
      when: never
    - if: '$CI_COMMIT_BRANCH == "master"'
    - if: '$CI_COMMIT_BRANCH == "main"'

preprod:
  stage: preprod
  script:
    - ansible --inventory hosts/production all -m ping
  rules:
    - if: '$CI_PIPELINE_SOURCE == "schedule"'
      when: never
    - if: '$CI_COMMIT_BRANCH == "production"'

provision:
  stage: provision
  trigger:
    include: trellis/.gitlab-ci.yml
  rules:
    - if: '$CI_PIPELINE_SOURCE == "schedule" && $CI_PIPELINE_SOURCE == "merge_request_event"'
      when: never
    - if: '$CI_COMMIT_BRANCH == "production"'
    - changes:
      - trellis/*

deploystaging:
  stage: deploystaging
  script:
    - ansible-playbook deploy.yml -e "site=${STAGING_DOMAIN} env=staging"
  rules:
    - if: '$CI_PIPELINE_SOURCE == "schedule"'
      when: never
    - if: '$CI_COMMIT_BRANCH == "master"'
    - if: '$CI_COMMIT_BRANCH == "main"'

deployprod:
  stage: deployprod
  script:
    - ansible-playbook deploy.yml -e "site=${PRODUCTION_DOMAIN} env=production"
  rules:
    - if: '$CI_PIPELINE_SOURCE == "schedule"'
      when: never
    - if: '$CI_COMMIT_BRANCH == "production"'
